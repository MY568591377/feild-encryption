package com.example.feild.encryption.domain.controller;

import cn.shuibo.annotation.Decrypt;
import cn.shuibo.annotation.Encrypt;
import com.alibaba.fastjson.JSON;
import com.example.feild.encryption.domain.pojo.dto.ResponseEntity;
import com.example.feild.encryption.domain.pojo.dto.SuccessResponseEntity;
import com.example.feild.encryption.domain.pojo.model.EncryptionExample;
import com.example.feild.encryption.domain.service.EncryptionExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author: 阿力
 * @date: 2020/5/20 10:56
 * @describe:
 */
@RestController
@RequestMapping("test")
public class TestController {
    @Autowired
    private EncryptionExampleService encryptionExampleService;

    @Encrypt
    @GetMapping("/encryption")
    public ResponseEntity<EncryptionExample> encryption() {
        EncryptionExample entity = encryptionExampleService.queryByMobile("13121152642");
        return new SuccessResponseEntity<>(entity);
    }

    @Decrypt
    @PostMapping("/decryption")
    public ResponseEntity<String> Decryption(@RequestBody String DESC) {
        System.out.println("=====================================");
        System.out.println("===============安达市大所多======================");
        return new SuccessResponseEntity<>(DESC);
    }

    @PostMapping("/ali")
    public ResponseEntity<String> ali(@RequestBody String DESC) {
        return new SuccessResponseEntity<>(DESC);
    }
}
