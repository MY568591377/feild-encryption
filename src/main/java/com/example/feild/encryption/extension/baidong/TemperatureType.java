package com.example.feild.encryption.extension.baidong;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * 储存温区
 *
 * @author zhangli
 * @date 2020/9/28
 */
@Getter
@AllArgsConstructor
public enum TemperatureType {
    /**
     *
     */
    FREEZING(10, "冷冻"),
    COLD_STORAGE(11, "冷藏"),
    NORMAL_TEMPERATURE(20, "常温"),
    CONSTANT_TEMPERATURE(21, "恒温"),
    ;

    private final Integer value;
    private final String desc;

    public static TemperatureType getByValue(Integer value) {
        return Arrays.stream(TemperatureType.values()).filter(x -> Objects.equals(x.getValue(), value)).findFirst().get();
    }

    public static TemperatureType getByDesc(String desc) {
        return Arrays.stream(TemperatureType.values()).filter(x -> Objects.equals(x.getDesc(), desc)).findFirst().get();
    }
}
