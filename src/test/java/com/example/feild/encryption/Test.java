package com.example.feild.encryption;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.feild.encryption.extension.util.DataCoderUtil;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.util.StopWatch;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class Test {

    public static void main(String[] args) throws InvalidFormatException, IOException {
        String a = "3";
        FLAG:
        if(a.equals("1")) {
            System.out.println("错误一");
        } else {
            if(a.equals("2")) {
                System.out.println("错误二");
                break FLAG;
            }

            if(a.equals("3")) {
                System.out.println("错误三");
                break FLAG;
            }

            if(a.equals("4")) {
                System.out.println("错误四");
                break FLAG;
            }
        }
        System.out.println("结束啦");
    }

    public static void lenghTest() {
        List<String> list = new ArrayList<>();
        for (int i = 1; i <= 50; i++) {
            String str = RandomStringUtils.randomAlphanumeric(i);

            System.out.println("原文：" + str + "\t长度：" + str.length());

            // 加密数据
            String data = DataCoderUtil.getInstance().encrypt(str);
            System.out.println("加密后：" + data + "\t长度：" + data.length());
            list.add("原长度：" + str.length() + "，加密长度：" + data.length() + "，长度差：" + (data.length() - str.length()));

            // 解密数据
            data = DataCoderUtil.getInstance().decrypt(data);
            System.out.println("解密后：" + data);
            System.out.println("==========================");
        }

        for (String string : list) {
            System.out.println(string);
        }
    }

    public static void decryptTest(String filePath) throws InvalidFormatException, IOException {
        StopWatch sw = new StopWatch();
        sw.start();
        DataCoderUtil.getInstance().new ImportExcel(filePath).decryptAndSave();
        sw.stop();

        System.out.println("--------------------------------------------------------");
        System.out.printf("------------Service is started. cost:%s s---------------\n", sw.getTotalTimeSeconds());
        System.out.println("--------------------------------------------------------");
    }

    public static void testJDK8() {
        List<Map<String, Object>> purchasePlanGoods = new ArrayList<Map<String, Object>>() {{
            add(new HashMap<String, Object>() {{
                put("id", 25);
                put("name", "18");
                put("desc", "33");
            }});

            add(new HashMap<String, Object>() {{
                put("id", 25);
                put("name", "45");
                put("desc", "jdisn");
                put("sxf", "jsn");
                put("acd", "124");
            }});

            add(new HashMap<String, Object>() {{
                put("id", 33);
                put("name", "45");
            }});
            add(new HashMap<String, Object>() {{
                put("id", 18);
                put("name", "45");
            }});
        }};
        TreeMap<Integer, List<Map<String, Object>>> matchsListMap = purchasePlanGoods.stream().collect(Collectors.groupingBy(x -> (Integer) x.get("id"), TreeMap::new, Collectors.toList()));
        for (Integer key : matchsListMap.descendingMap().keySet()) {
            System.out.println(key);
        }
    }
}
