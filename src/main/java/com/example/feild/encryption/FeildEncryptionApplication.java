package com.example.feild.encryption;

import cn.shuibo.annotation.EnableSecurity;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableScheduling
@ServletComponentScan
@EnableSecurity
public class FeildEncryptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeildEncryptionApplication.class, args);
	}
}
