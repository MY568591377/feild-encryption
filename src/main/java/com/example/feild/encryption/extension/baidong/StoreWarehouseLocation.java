package com.example.feild.encryption.extension.baidong;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;

public class StoreWarehouseLocation {
    private static final String httpUrl = "http://cp.diancang.pro/wisp-storage/storeWarehouseLocation/add";
    private static final String TOKEN = "0783095b-54d8-4ea7-995e-2acaee2f3c2d";
    private static final String USER_ID = "76feace404f000";

    public static void main(String[] args) {
        for (String data : getData()) {
            post(data.split(","));
        }
    }


    private static void post(String[] array) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName("utf-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("token", TOKEN);
        headers.set("user-id", USER_ID);

        StoreWarehouseLocationDTO dto = new StoreWarehouseLocationDTO();
        dto.setWarehouseLocationName(array[0]);
        dto.setWarehouseId(33495628344758272L);
        dto.setWarehouseRegionId(33667068478427136L);
        dto.setWarehouseLocationType(getLocationType(array[1]));

        HttpEntity<StoreWarehouseLocationDTO> requestEntity = new HttpEntity<StoreWarehouseLocationDTO>(dto, headers);
        ResponseEntity<JSONObject> response = restTemplate.postForEntity(httpUrl, requestEntity, JSONObject.class);
        System.out.println(response.getBody());
        Assert.isTrue(Objects.equals(response.getBody().getInteger("code"), 200), "数据异常");
    }

    static Integer getLocationType(String name) {
        if (Objects.equals(name, "拣货库位")) {
            return 10;
        } else if (Objects.equals(name, "存货库位")) {
            return 20;
        }
        return null;
    }

    @Data
    static class StoreWarehouseLocationDTO {
        @NotEmpty
        @Length(max = 32)
        @ApiModelProperty(value = "库位名称")
        private String warehouseLocationName;

        @NotNull
        @ApiModelProperty(value = "所属仓库ID")
        private Long warehouseId;

        @NotNull
        @ApiModelProperty(value = "库区ID")
        private Long warehouseRegionId;

        @ApiModelProperty(value = "库位使用")
        private Integer warehouseLocationType;

        @ApiModelProperty(value = "货主标签ID")
        private Long goodsOwnerId;

        @ApiModelProperty(value = "备注")
        private String warehouseLocationDesc;
    }

    private static List<String> getData() {
        List<String> list = Lists.newLinkedList();
        list.add("01-01-1,拣货库位");
        list.add("01-02-1,拣货库位");
        list.add("01-03-1,拣货库位");
        list.add("01-04-1,拣货库位");
        list.add("01-05-1,拣货库位");
        list.add("01-06-1,拣货库位");
        list.add("01-07-1,拣货库位");
        list.add("01-08-1,拣货库位");
        list.add("01-09-1,拣货库位");
        list.add("01-10-1,拣货库位");
        list.add("01-11-1,拣货库位");
        list.add("01-12-1,拣货库位");
        list.add("01-13-1,拣货库位");
        list.add("01-14-1,拣货库位");
        list.add("01-15-1,拣货库位");
        list.add("01-16-1,拣货库位");
        list.add("01-17-1,拣货库位");
        list.add("01-18-1,拣货库位");
        list.add("01-19-1,拣货库位");
        list.add("01-20-1,拣货库位");
        list.add("11-01-1,存货库位");
        list.add("11-02-1,存货库位");
        list.add("11-03-1,存货库位");
        list.add("11-04-1,存货库位");
        list.add("11-05-1,存货库位");
        list.add("11-06-1,存货库位");
        list.add("11-07-1,存货库位");
        list.add("11-08-1,存货库位");
        list.add("11-09-1,存货库位");
        list.add("11-10-1,存货库位");
        return list;
    }
}
