package com.example.feild.encryption.extension.util;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

public class HttpUtil {
    private static final String httpUrl = "http://gateway-test.c0772d40e2b644f9daa5cc6b143edec9d.cn-hangzhou.alicontainer.com/wisp-auth/storeResource/functionGroup/add";
    private static  final String TOKEN = "56a2ea92-cb8d-4c9f-8db6-95460355f138";
    private static  final String USER_ID = "6797279885f000";

    public static void main(String[] args) {
        List<String> list = new LinkedList<>();
        for (String s : list) {
            post(s.split(","));
        }
    }

    public static void post(String[] array) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName("utf-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("token", TOKEN);
        headers.set("user-id", USER_ID);
        JSONObject params = new JSONObject();
        params.put("parentResourceId", array[0].toString());
        params.put("resourceName", array[1].toString());
        params.put("routingUrl", array[1].toString());
        HttpEntity<JSONObject> requestEntity = new HttpEntity<JSONObject>(params, headers);
        ResponseEntity<JSONObject> response = restTemplate.postForEntity(httpUrl, requestEntity, JSONObject.class);
        System.out.println(response.getBody());
    }
}
