package com.example.feild.encryption.domain.dao.mappers;

import com.example.feild.encryption.domain.pojo.model.EncryptionExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface EncryptionExampleMapper {
	EncryptionExample queryByMobile(@Param("mobile") String mobile);

	List<EncryptionExample> pagingQuery();

	int addEntity(EncryptionExample entity);

	int updateEntity(EncryptionExample entity);
}
