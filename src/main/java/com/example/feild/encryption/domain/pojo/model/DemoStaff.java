package com.example.feild.encryption.domain.pojo.model;

import java.util.Date;
import java.io.Serializable;
import javax.persistence.*;

/**
 * (DemoStaff)表数据库访问层
 * @author makejava
 * @since 2020-04-30 16:03:26
 */
@Table(name = "demo_staff")
public class DemoStaff implements Serializable {
    private static final long serialVersionUID = 241201544827751010L;
    @Id
    @Column(name = "staff_id")
    private Integer staff_id;
    /** 姓名 */
    @Column(name = "name")
    private String name;
    /** 年龄 */
    @Column(name = "age")
    private Integer age;
    /** 创建时间 */
    @Column(name = "create_time")
    private Date create_time;
    /** 修改时间 */
    @Column(name = "update_time")
    private Date update_time;

    public Integer getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(Integer staff_id) {
        this.staff_id = staff_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }
}