package com.example.feild.encryption;

import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class A {
    public static void main(String[] args) throws JSONException {
        List<StoreWarehouseLocation> list = Lists.newLinkedList();
        list.add(StoreWarehouseLocation.builder().quantity(BigDecimal.valueOf(10.5)).build());
        list.add(StoreWarehouseLocation.builder().quantity(null).build());
        list.add(StoreWarehouseLocation.builder().quantity(BigDecimal.valueOf(12)).build());
        System.out.println(list.stream().map(StoreWarehouseLocation::getQuantity).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add));
    }

    /**
     * 库位
     *
     * @author chenyuwen
     */
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class StoreWarehouseLocation {
        /**
         * 库位类型(10:拣货库位;20:存货库位)
         */
        @ApiModelProperty("库位类型(10:拣货库位;20:存货库位)")
        private BigDecimal quantity;
    }
}
