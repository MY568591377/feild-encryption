# docker简单入门



### 1、前言

大家好，我是本次主讲人陈玉文。这是我们2021年第一场培训。以后的培训大致会延续本次培训课件风格，以“5W1H”去引导思考。

1、what(什么)	docker是什么？

2、why(为什么)	为什么要使用docker？

3、when(何时)	什么时候使用docker？程序部署时

4、where(何处)	  哪里需要使用docker？服务器上

5、who(谁)	 谁使用docker？服务器

6、how(怎样)	docker怎么使用？

### 2、docker是什么？

容器化技术。

java:"一次编译，到处运行"

docker:"一次构建，到处运行"

Docker 使用 Google 公司推出的 Go 语言 进行开发实现，基于 Linux 内核的
cgroup，namespace，以及 AUFS 类的 Union FS 等技术，对进程进行封装隔离，属于 操作
系统层面的虚拟化技术。由于隔离的进程独立于宿主和其它的隔离的进程，因此也称其为容
器。

虚拟机和docker对比

![img](https://dc-uploadfile.oss-cn-hangzhou.aliyuncs.com/chenyuwen/20006deca0fccda0d536edd626835e9e_720w.jpg)

物理机

![img](https://dc-uploadfile.oss-cn-hangzhou.aliyuncs.com/chenyuwen/v2-2a05f6b780ea9692a459477bbda7b066_720w.jpg)

虚拟机

![img](https://dc-uploadfile.oss-cn-hangzhou.aliyuncs.com/chenyuwen/v2-a4bd3589de0a0536d73fd53f3403adff_720w.jpg)

docker容器

![img](https://dc-uploadfile.oss-cn-hangzhou.aliyuncs.com/chenyuwen/v2-b9430887dc90820af97bb2d35854e34d_720w.jpg)

基本概念

![image-20210103152621583](https://dc-uploadfile.oss-cn-hangzhou.aliyuncs.com/chenyuwen/image-20210103152621583.png)

类似概念：maven、npm、git

![image-20210103152727012](https://dc-uploadfile.oss-cn-hangzhou.aliyuncs.com/chenyuwen/image-20210103152727012.png)

![image-20210103153026828](https://dc-uploadfile.oss-cn-hangzhou.aliyuncs.com/chenyuwen/image-20210103153026828.png)



### 3、为什么要使用docker？

| 特性       | 容器               | 虚拟机     |
| ---------- | ------------------ | ---------- |
| 启动       | 秒级               | 分钟级     |
| 硬盘使用   | 一般为  MB         | 一般为  GB |
| 性能       | 接近原生           | 弱于       |
| 系统支持量 | 单机支持上千个容器 | 一般几十个 |

1. 更高效的利用系统资源
2. 更快速的启动时间
3. 一致的运行环境
4. 持续交付和部署
5. 更轻松的迁移
6. 更轻松的维护和扩展

### 4、docker怎么使用？

##### 4.1、docker安装

1. 卸载旧版本

   ```
   sudo yum remove docker \
                     docker-client \
                     docker-client-latest \
                     docker-common \
                     docker-latest \
                     docker-latest-logrotate \
                     docker-logrotate \
                     docker-engine
   ```

2. 安装所需软件包

   提示：centos8默认使用podman代替docker，所以需要containerd.io

   ```
   yum install -y https://download.docker.com/linux/fedora/30/x86_64/stable/Packages/containerd.io-1.2.6-3.3.fc30.x86_64.rpm
   ```

   ```
   sudo yum install -y yum-utils \
     device-mapper-persistent-data \
     lvm2
   ```

3. 设置软件源地址

   ```
   sudo yum-config-manager \
       --add-repo \
       https://download.docker.com/linux/centos/docker-ce.repo
   ```

4. 安装docker

   ```
   sudo yum install -y docker-ce docker-ce-cli containerd.io
   ```

5. 配置设置

   创建或修改 /etc/docker/daemon.json 文件

   设置仓库源和软件数据存储路径(根据磁盘可用空间填写)

   ```json
   { 
       "registry-mirrors" : [
   		"https://registry.docker-cn.com",
   		"https://docker.mirrors.ustc.edu.cn",
   		"http://hub-mirror.c.163.com",
   		"https://cr.console.aliyun.com/"
   	],
    	"data-root": "/home/docker-data"
   } 
   ```

6. 启动docker

   ```
   sudo systemctl start docker
   ```

7. 设置开机启动

   ```
   sudo systemctl enable docker
   ```
   
8. 验证

   ```
   sudo docker run hello-world
   ```

##### 4.2、镜像基本命令

1. 获取镜像

   ```
   docker pull
   ```

2. 列出镜像

   ```
   docker images
   ```

3. 删除本地镜像

   ```
   docker image rm
   ```

4. 制作镜像Dockerfile

   ```shell
   FROM openjdk:8-jdk
   ENV TZ=Asia/Shanghai
   COPY target/*.jar app.jar
   CMD java -Dfile.encoding=utf-8 -jar app.jar
   EXPOSE 8080
   
   FROM nginx:1.19.2
   ENV TZ=Asia/Shanghai
   COPY nginx.conf /etc/nginx/nginx.conf
   COPY dist dist
   EXPOSE 80
   ```

##### 4.3、容器基本命令

1. 启动

   ```
   docker run
   -i==--input       让容器的标准输入保持打开
   -t==--terminal    让Docker分配一个伪终端（pseudo-tty）并绑定到容器的标准输入
   -v==--volume      挂载卷
   -p==--port        端口映射
   -e==--environment 环境变量
   -d==--daemon      守护进程
   -- name 容器名
   -- restart always 保持重启
   ```

2. 守护态运行

   ```
   docker run -d
   ```

3. 终止

   ```
   docker stop
   ```

4. 进入容器

   ```
   docker exec -it ID bash
   ```

5. 删除

   ```
   docker rm
   ```

### 5、附录

##### 5.1、几个容器运行示例

1. mysql

   ```shell
   docker run \
     	--name mysql \
     	--restart always \
     	-p 3306:3306 \
     	-v /docker/mysql/conf:/etc/mysql/conf.d \
     	-v /docker/mysql/logs:/logs \
     	-v /docker/mysql/data:/var/lib/mysql \
     	-e MYSQL_ROOT_PASSWORD=123456 \
     	-d mysql:5.7
   ```

2. mysql集群

   ```shell
   docker network create -d bridge my-net
   docker run \
   	--name=mysql-node-1 \
   	--restart always \
   	--net=mysql-net \
   	--privileged \
   	-p 4306:3306 \
   	-v v1:/var/lib/mysql \
   	-e MYSQL_ROOT_PASSWORD=123456 \
   	-e CLUSTER_NAME=PXC \
   	-e XTRABACKUP_PASSWORD=123456 \
   	-d percona/percona-xtradb-cluster:8.0
   docker run \
   	--name=mysql-node-2 \
   	--restart always \
   	--net=mysql-net \
   	--privileged \
   	-p 4307:3306 \
   	-v v2:/var/lib/mysql \
   	-e MYSQL_ROOT_PASSWORD=123456 \
   	-e CLUSTER_NAME=PXC \
   	-e XTRABACKUP_PASSWORD=123456 \
   	-e CLUSTER_JOIN=mysql-node-1 \
   	-d percona/percona-xtradb-cluster:8.0
   ```

3. portainer

   ```shell
   docker run \
   	--name portainer \
   	--restart always \
   	-p 9000:9000 \
   	-v /var/run/docker.sock:/var/run/docker.sock \
   	-v /docker/portainer/data:/data \
   	-d portainer/portainer-ce
   ```

4. gitlab

   ```shell
   docker run \
       --name gitlab \
       --restart always \
       --privileged true \
       --hostname 192.168.1.88 \
       -p 80:80 \
       -p 443:443 \
       -p 222:22 \
       -v /docker/gitlab/config:/etc/gitlab \
       -v /docker/gitlab/logs:/var/log/gitlab \
       -v /docker/gitlab/data:/var/opt/gitlab \
       -d gitlab/gitlab-ce
   ```

5. redis

   ```shell
   docker run \
     	--name redis \
     	--restart always \
     	-p 6379:6379 \
     	-v /docker/redis.conf:/etc/redis/redis.conf \
     	-v /docker/redis/data:/data \
     	-d redis redis-server \
     	--requirepass "123456" \
     	--appendonly yes
   ```

6. elasticsearch

   ```shell
   docker run \
     	--name elasticsearch \
     	--restart always \
     	-p 9200:9200 \
     	-p 9300:9300 \
     	-e "discovery.type=single-node" \
     	-d elasticsearch:7.9.0
   ```
   
7. kibana

   ```shell
   docker run \
   	--name kibana \
   	--restart always \
   	-p 5601:5601 \
   	-e ELASTICSEARCH_HOSTS=http://172.16.121.213:9200 \
   	-d kibana:7.9.0
   ```

8. skywalking-aop-server

   ```shell
   docker run \
     	--name oap \
     	--restart always \
     	-p 11800:11800 \
     	-p 12800:12800 \
     	-e SW_STORAGE=elasticsearch7 \
     	-e SW_STORAGE_ES_CLUSTER_NODES=172.16.121.213:9200 \
     	-d apache/skywalking-oap-server
   ```

9. skywalking-ui

   ```shell
   docker run \
   	--name skywalking-ui \
   	--restart always \
   	-p 12345:8080 \
   	-e SW_OAP_ADDRESS=172.16.121.213:12800 \
   	-d apache/skywalking-ui
   ```

10. jenkins

   ```shell
   docker run \
   	--name jenkins \
   	--restart always \
   	--privileged true  \
   	-p 8080:8080 \
   	-p 50000:50000 \
   	-v /docker/jenkins:/var/jenkins_home  \
   	-v /var/run/docker.sock:/var/run/docker.sock \
   	-v /etc/localtime:/etc/localtime \
   	-v /usr/bin/docker:/usr/bin/docker \
   	-u root \
   	-d jenkins/jenkins
   ```

11. mongo

    ```shell
    docker run \
    	--name mongo \
    	--restart always \
    	-p 27017:27017 \
    	-v /docker/mongodb/data:/data/db \
    	-d mongo
    ```

12. rabbitmq

    ```shell
    docker run \
    	--name rabbitmq \
    	--restart always \
    	-v /docker/rabbitmq/data:/var/lib/rabbitmq \
    	-p 15672:15672 \
    	-p 5672:5672 \
    	-d rabbitmq:management
    ```

13. activemq

    ```shell
    docker run \
    	--name activemq \
    	--restart always \
    	-p 61616:61616 \
    	-p 8161:8161 \
    	-d webcenter/activemq
    ```

14. nexus(需要目录权限)

    ```shell
    docker run  \
      	--name nexus \
      	--restart always \
      	--privileged true \
      	-p 8081:8081 \
      	-v /docker/nexus/data:/nexus-data \
      	-d sonatype/nexus3
    ```

15. nginx

    ```shell
    docker run \
      	--name nginx \
      	--restart always \
      	-p 12345:80 \
      	-v /docker/nginx/conf/nginx.conf:/etc/nginx/nginx.conf \
      	-v /docker/web/data:/web \
      	-v /docker/nginx/log:/var/log/nginx \
      	-d nginx
    ```

##### 5.2、参考资料

[docker-从入门到实践](https://dc-uploadfile.oss-cn-hangzhou.aliyuncs.com/chenyuwen/pdf/docker_practice.pdf)

软件包拓展

```
yum -y install epel-release
yum -y install htop
```

##### 5.3、培训作业

参与培训人员均需在服务器上创建以自己名字命名的镜像，并能成功运行容器，具有回声功能

回声：返回和输入一致。后端采用接口返回的方式，前端采用输入框和显示界面的方式

