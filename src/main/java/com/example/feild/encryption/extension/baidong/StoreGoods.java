package com.example.feild.encryption.extension.baidong;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;

/**
 * 商品
 */
public class StoreGoods {
    private static final String httpUrl = "http://cp.diancang.pro/wisp-system/storeGoods/add";
    private static final String TOKEN = "0a5bc6cc-8272-4c28-8393-d814ba01d8b2";
    private static final String USER_ID = "76feace404f000";

    public static void main(String[] args) {
        for (String data : getData()) {
            post(data.split(","));
        }
    }


    private static void post(String[] array) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName("utf-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("token", TOKEN);
        headers.set("user-id", USER_ID);

        StoreGoodsDTO o = new StoreGoodsDTO();
        o.setGoodsName(StringUtils.trim(array[0]));
        o.setGoodsCategoryId(AreaData.getCategory(array[1]));
        o.setGoodsUnit(StringUtils.trim(array[2]));
        o.setGoodsSpec(StringUtils.trim(array[3]));
//        String validDays = StringUtils.trim(array[5]);
//        if (StringUtils.isNoneEmpty(validDays)) {
//            o.setValidDays(Integer.valueOf(validDays));
//        }
        o.setTemperatureType(TemperatureType.getByDesc(array[4]).getValue());

        HttpEntity<StoreGoodsDTO> requestEntity = new HttpEntity<StoreGoodsDTO>(o, headers);
        ResponseEntity<JSONObject> response = restTemplate.postForEntity(httpUrl, requestEntity, JSONObject.class);
        System.out.println(response.getBody());
        Assert.isTrue(Objects.equals(response.getBody().getInteger("code"), 200), "数据异常");
    }

    private static List<String> getData() {
        List<String> list = Lists.newLinkedList();
        list.add("红枣枸杞银耳羹（混用）,海鲜类,箱,100包/箱,常温");
        list.add("百合银耳荔枝羹（混用）,海鲜类,箱,100包/箱,常温");
        list.add("汤桶+汤盖（大有上九）,海鲜类,箱,500个/箱,常温");
        list.add("塑料汤杯（季南熙）,海鲜类,箱,200套/箱,常温");
        list.add("拉拉袋（季南熙）,打包盒,箱,1000个/箱,常温");
        list.add("一次性餐包（季南熙）,打包盒,箱,1000个/箱,常温");
        list.add("奥尔良鸡全腿（大有上九）,海鲜类,箱,10包/箱,冷冻");
        list.add("土豆饼（大有上九）,海鲜类,箱,120个/箱,冷冻");
        list.add("鳕鱼排（大有上九）,海鲜类,箱,20包/箱,冷冻");
        list.add("猪排（大有上九）,猪肉,箱,10包/箱,冷冻");
        return list;
    }

    @Data
    @ApiModel("商品信息表入参对象")
    static class StoreGoodsDTO {
        @NotEmpty
        @Length(max = 64)
        @ApiModelProperty(value = "商品名称")
        private String goodsName;

        @NotNull
        @ApiModelProperty(value = "商品品类ID")
        private Long goodsCategoryId;

        @ApiModelProperty(value = "商品分组ID")
        private Long goodsGroupId;

        @NotEmpty
        @ApiModelProperty(value = "单位")
        private String goodsUnit;

        @NotEmpty
        @ApiModelProperty(value = "规格")
        private String goodsSpec;

        @ApiModelProperty(value = "有效期(天)")
        private Integer validDays;

        @ApiModelProperty(value = "临期(天)")
        private Integer adventDays;

        @ApiModelProperty(value = "净重(g)")
        private Integer netWeight;

        @ApiModelProperty(value = "毛重(g)")
        private Integer grossWeight;

        @ApiModelProperty(value = "长(cm)")
        private Integer goodsLength;

        @ApiModelProperty(value = "宽(cm)")
        private Integer goodsWidth;

        @ApiModelProperty(value = "高(cm)")
        private Integer goodsHeight;

        @ApiModelProperty(value = "体积(dm³)")
        private String volume;

        @ApiModelProperty(value = "板堆叠(个)")
        private Integer goodsQuantity;

        @ApiModelProperty(value = "储存温区(10:冷冻;11:冷藏;20:常温;21:恒温)")
        private Integer temperatureType;

        @ApiModelProperty(value = "商品图片地址")
        private String goodsPicture;
    }
}
