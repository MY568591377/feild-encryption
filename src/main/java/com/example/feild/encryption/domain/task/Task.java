package com.example.feild.encryption.domain.task;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author: 阿力
 * @date: 2019/12/23 18:47
 * @describe:
 */
@Component
public class Task {
    public static final BlockingQueue<JSONObject> bq = new LinkedBlockingQueue<JSONObject>() {{
        add(new JSONObject() {{
            put("api_order_code", "1CNHea009906686");
        }});
        add(new JSONObject() {{
            put("api_order_code", "1CNHea069497877");
        }});
    }};

//    @Scheduled(cron = "0 0/6 * * * ?")
    public void po() {
        String url = "http://192.168.100.124:10013/third/part/callback/shuhai/CreateOrder/push";
        RestTemplate restTemplate = new RestTemplate();
        String resp = restTemplate.postForEntity(url, bq.poll(), String.class).getBody();
        System.out.println(resp);
    }

    public static void main(String[] args) {
        RestTemplate restTemplate = new RestTemplate();

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("no", "TT7700207571467");
        map.add("type", "");

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        headers.set("Authorization", "APPCODE " + "12f4c02e170c44e881f641ced427aad0");

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(map, headers);
        ResponseEntity<String> response = restTemplate.exchange("https://wuliu.market.alicloudapi.com/kdi", HttpMethod.GET, requestEntity, String.class);
        String sttr = response.getBody();
        System.out.println(sttr);
    }
}
