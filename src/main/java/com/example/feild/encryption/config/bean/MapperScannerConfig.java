package com.example.feild.encryption.config.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tk.mybatis.spring.mapper.MapperScannerConfigurer;

import java.util.Properties;

/**
 * @author: 阿力
 * @date: 2020/4/27 17:05
 * @describe:
 */
@Configuration
public class MapperScannerConfig {
    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer msc = new MapperScannerConfigurer();
        msc.setSqlSessionFactoryBeanName("sqlSessionFactory");
        msc.setBasePackage("com.example.feild.encryption.domain.dao.mappers");
        Properties properties = new Properties();
        properties.setProperty("mappers", "com.example.feild.encryption.domain.dao.CommonMapper");
        properties.setProperty("plugins", "com.github.pagehelper.PageInterceptor");
        msc.setProperties(properties);
        return msc;
    }
}
