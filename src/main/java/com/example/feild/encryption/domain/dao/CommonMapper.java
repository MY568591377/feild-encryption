package com.example.feild.encryption.domain.dao;

import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.UpdateProvider;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.ids.DeleteByIdsMapper;
import tk.mybatis.mapper.common.special.InsertListMapper;

import java.util.List;

/**
 * @author: 阿力
 * @date: 2020/4/27 14:37
 * @describe:
 */
public interface CommonMapper<T> extends Mapper<T>, InsertListMapper<T>, DeleteByIdsMapper<T> {
    @SuppressWarnings("MybatisMapperMethodInspection")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @UpdateProvider(type = CommonProvider.class, method = "dynamicSQL")
    int batchUpdateByPrimaryKey(List<T> recordList);

    @SuppressWarnings("MybatisMapperMethodInspection")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @UpdateProvider(type = CommonProvider.class, method = "dynamicSQL")
    int batchUpdateByPrimaryKeySelective(List<T> recordList);
}
