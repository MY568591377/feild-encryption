package com.example.feild.encryption.extension.baidong;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.List;

/**
 * 采购单
 */
public class CustomerOrder {
    private static final String httpUrl = "http://cp.diancang.pro/wisp-order/storeCustomerOrder/add";
    private static final String TOKEN = "0a5bc6cc-8272-4c28-8393-d814ba01d8b2-0000";
    private static final String USER_ID = "76feace404f000AAAA";

    public static void main(String[] args) {
        for (String data : getData()) {
            post(data.split("，"));
        }
    }


    private static void post(String[] array) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName("utf-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("token", TOKEN);
        headers.set("user-id", USER_ID);

        Object o = null;

        HttpEntity<Object> requestEntity = new HttpEntity<>(o, headers);
        ResponseEntity<JSONObject> response = restTemplate.postForEntity(httpUrl, requestEntity, JSONObject.class);
        System.out.println(response.getBody());
    }

    @Data
    @ApiModel("订货单入参对象")
    static class StoreCustomerOrderDTO {
        @NotNull
        @ApiModelProperty(value = "货主ID")
        private Long goodsOwnerId;

        @NotNull
        @ApiModelProperty(value = "仓库ID")
        private Long warehouseId;

        @NotNull
        @ApiModelProperty(value = "收货人")
        private Long customerId;

        @Length(max = 30)
        @ApiModelProperty(value = "自定义单号")
        private String customerOrderCode;

        @ApiModelProperty(value = "备注")
        private String customerOrderDesc;

        @Valid
        @NotEmpty
        @ApiModelProperty(value = "商品列表信息")
        private List<@NotNull CustomerWarehouseGoodsDTO> warehouseGoodsList;

        @Data
        @ApiModel("订货单商品信息")
        static class CustomerWarehouseGoodsDTO {
            @NotNull
            @ApiModelProperty(value = "仓库商品ID")
            private Long warehouseGoodsId;

            @Min(1)
            @NotNull
            @ApiModelProperty(value = "件数")
            private Integer orderQuantity;

            @DecimalMin("0.01")
            @NotNull
            @ApiModelProperty(value = "单价(元)")
            private BigDecimal goodsUnitPrice;
        }
    }

    private static List<String> getData() {
        List<String> list = Lists.newLinkedList();
        list.add("江苏徐州店，江苏省徐州市泉山区中山北路6号金地商都东侧北楼102室商铺一酸一辣酸菜牛肉粉，王茹，13401279249");
        return list;
    }
}
