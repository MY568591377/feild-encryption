package com.example.feild.encryption.extension.baidong;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.nio.charset.Charset;
import java.util.List;

/**
 * 收货人信息批量导入
 */
public class Receiver {
    private static final String httpUrl = "http://cp.diancang.pro/wisp-auth/storeReceiver/add";
    private static final String TOKEN_PRE = "9c398824-1706-4370-837f-69b404d335ec";
    private static final String TOKEN_PROD = "0a5bc6cc-8272-4c28-8393-d814ba01d8b2";
    private static final String USER_ID = "76feace404f000";

    public static void main(String[] args) {
        for (String data : getData()) {
            post(data.split(","));
        }
    }


    private static void post(String[] array) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName("utf-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("token", TOKEN_PROD);
        headers.set("user-id", USER_ID);

        StoreWarehouseLocationDTO dto = new StoreWarehouseLocationDTO();
        dto.setWarehouseLocationName(array[1]);
        dto.setWarehouseId(33495628344758272L);
        dto.setWarehouseRegionId(33667068478427136L);
        if(StringUtils.isNotEmpty(array[2])) {
            dto.setWarehouseLocationType(Integer.valueOf(array[2]));
        }

        HttpEntity<StoreWarehouseLocationDTO> requestEntity = new HttpEntity<>(dto, headers);
        ResponseEntity<JSONObject> response = restTemplate.postForEntity(httpUrl, requestEntity, JSONObject.class);
        System.out.println(response.getBody());
    }

    @Data
    static class StoreWarehouseLocationDTO {
        @NotEmpty
        @Length(max = 32)
        @ApiModelProperty(value = "库位名称")
        private String warehouseLocationName;

        @NotNull
        @ApiModelProperty(value = "所属仓库ID")
        private Long warehouseId;

        @NotNull
        @ApiModelProperty(value = "库区ID")
        private Long warehouseRegionId;

        @ApiModelProperty(value = "库位使用")
        private Integer warehouseLocationType;

        @ApiModelProperty(value = "货主标签ID")
        private Long goodsOwnerId;

        @ApiModelProperty(value = "备注")
        private String warehouseLocationDesc;
    }

    private static List<String> getData() {
        List<String> list = Lists.newLinkedList();
        list.add("大有上九·超级牛腩煲(龙汇路店),马国富,13817631394,上海市/直辖市/浦东新区,龙汇路316号209室");
        list.add("大有上九·超级牛腩煲(宝山万达店),苏尚兵,15026897254,上海市/直辖市/宝山区,长临路959弄38号206室");
        list.add("大有上九·超级牛腩煲(肇家浜路店),贺天路,15221406297,上海市/直辖市/徐汇区,肇嘉浜路333号地下商城A区（牛家人大碗牛肉面隔壁）");
        list.add("大有上九·超级牛腩煲(七宝店),王超,17621202211,上海市/直辖市/闵行区,中谊路525号(格林豪泰酒店旁)");
        list.add("大有上九·超级牛腩煲(高科西路店),高威,15026528653,上海市/直辖市/浦东新区,东方路3601号2号楼2034室");
        list.add("大有上九·超级牛腩煲(五角场店),仲慧慧,15317841443,上海市/直辖市/杨浦区,国权路69号绿色米兰地下1层01-11室");
        list.add("大有上九·超级牛腩煲(新江湾城店),岳刚,18655090587,上海市/直辖市/杨浦区,国霞路418号1层何记花甲里面来");
        list.add("大有上九·超级牛腩煲(人民广场店),王浩,15240084426,上海市/直辖市/黄浦区,广东路555号嘉食荟美食城");
        list.add("大有上九·超级牛腩煲(航北店),冯坤银,13262971795,上海市/直辖市/闵行区,航北路29号");
        list.add("大有上九·超级牛腩煲(长风公园店),邹宇航,13166065075,上海市/直辖市/普陀区,金沙江路788号401-2、402、403室（熊猫星厨A6)");
        list.add("大有上九·超级牛腩煲(金桥店),袁威,18602124176,上海市/直辖市/浦东新区,长岛路1188号地下1_1层，1226号地下1层，1层01室");
        list.add("大有上九·超级牛腩煲(安远路店),江思明,19512304208,上海市/直辖市/普陀区,昌化路745号3层3F-1");
        list.add("大有上九·超级牛腩煲(外高桥店),马月梅,17321157831,上海市/直辖市/浦东新区,自由贸易试验区华京路7号A部位");
        list.add("大有上九·超级牛腩煲(莘庄店),任礼佳,13061951643,上海市/直辖市/闵行区,莘建路23-3（临）号二层");
        list.add("大有上九·超级牛腩煲(陆家嘴店),王东瑞,13166296889,上海市/直辖市/浦东新区,自由贸易试验区潍坊街道浦电路316号3层A室");
        list.add("大有上九·超级牛腩煲(江桥万达店),李福林,16621677973,上海市/直辖市/嘉定区,鹤旋路72号1 2层");
        list.add("大有上九·超级牛腩煲(漕宝路店),周向华,19512468790,上海市/直辖市/徐汇区,田林东路75号306-309号");
        list.add("大有上九·超级牛腩煲(灵石路店),张美娟,18301995546,上海市/直辖市/静安区,灵石路709号68幢102-107室、1幢709-11、709-15、1幢202-204室");
        return list;
    }
}
