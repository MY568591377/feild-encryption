package com.example.feild.encryption.domain.controller;

import com.example.feild.encryption.domain.pojo.dto.ResponseEntity;
import com.example.feild.encryption.domain.pojo.dto.SuccessResponseEntity;
import com.example.feild.encryption.domain.service.DemoStaffService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * (DemoStaff)表控制层
 * @author makejava
 * @since 2020-04-27 10:51:05
 */
@RestController
@RequestMapping("demoStaff")
public class DemoStaffController {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoStaffController.class);
    /**
     * 服务对象
     */
    @Autowired
    private DemoStaffService demoStaffService;

    @RequestMapping(value = "batchInsert", method = RequestMethod.GET)
    public ResponseEntity<String> batchInsert() {
        demoStaffService.batchInsert();
        return new SuccessResponseEntity<>();
    }

    @RequestMapping(value = "batchUpdate", method = RequestMethod.GET)
    public ResponseEntity<String> batchUpdate() {
        demoStaffService.batchUpdate();
        return new SuccessResponseEntity<>();
    }

    @RequestMapping(value = "batchDelete", method = RequestMethod.GET)
    public ResponseEntity<String> batchDelete() {
        demoStaffService.batchDelete();
        return new SuccessResponseEntity<>();
    }

    @GetMapping("ali")
    public String testLog() throws InterruptedException {
        LOGGER.info("测试一下数据{}", "DemoStaffController");
        Thread.currentThread().sleep(1000L);
        return "success";
    }
}