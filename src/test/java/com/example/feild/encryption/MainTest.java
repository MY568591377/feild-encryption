package com.example.feild.encryption;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.PropertyFilter;
import com.alibaba.fastjson.serializer.SerializeFilter;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * @author: 阿力
 * @date: 2019/12/26 14:51
 * @describe:
 */
public class MainTest {
    static class Person {
        String name;
        int age;
        String address;

        public Person() {
        }

        public Person(String name, int age, String address) {
            this.name = name;
            this.age = age;
            this.address = address;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }

    public static void main(String[] args) {
        List<Person> list = new ArrayList<Person>() {{
            add(new Person("zhangli", 30, "黄冈"));
            add(new Person("xiongjian", 26, "黄陂"));
            add(new Person("xm", 21, "黄石"));
        }};
        SimplePropertyPreFilter filter = new SimplePropertyPreFilter(Person.class, "name");
        System.out.println(JSON.toJSONString(list, filter));
    }
}
