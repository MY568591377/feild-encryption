package com.example.feild.encryption.extension.util;

import org.apache.commons.codec.digest.DigestUtils;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;

public class MD5Util {
    private static final String ALGORITHM = "MD5";

    public static String digest(String in) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(ALGORITHM);
            messageDigest.reset();
            byte[] digest = messageDigest.digest(in.getBytes("UTF-8"));
            return DatatypeConverter.printHexBinary(digest).toLowerCase();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        System.out.println(digest("123456"));
        //        System.out.println(org.apache.commons.codec.digest.DigestUtils.md5Hex("123456"));
        System.out.println(DigestUtils.md5Hex("123456"));
    }
}
