package com.example.feild.encryption.domain.dao.mappers;

import com.example.feild.encryption.domain.dao.CommonMapper;
import com.example.feild.encryption.domain.dao.CommonProvider;
import com.example.feild.encryption.domain.pojo.model.DemoStaff;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.UpdateProvider;
import tk.mybatis.mapper.provider.IdsProvider;
import tk.mybatis.mapper.provider.SpecialProvider;
import tk.mybatis.mapper.provider.SqlServerProvider;

import java.util.List;

/**
 * (DemoStaff)表数据库访问层
 * @author makejava
 * @since 2020-04-30 16:03:27
 */
public interface DemoStaffMapper extends CommonMapper<DemoStaff> {
    @SuppressWarnings("MybatisMapperMethodInspection")
    @Override
    @Options(useGeneratedKeys = true, keyProperty = "staff_id")
    @InsertProvider(type = SqlServerProvider.class, method = "dynamicSQL")
    int insertSelective(DemoStaff record);

    @SuppressWarnings("MybatisMapperMethodInspection")
    @Options(useGeneratedKeys = true, keyProperty = "staff_id")
    @InsertProvider(type = SpecialProvider.class, method = "dynamicSQL")
    int insertList(List<DemoStaff> recordList);

    @SuppressWarnings("MybatisMapperMethodInspection")
    @Options(useGeneratedKeys = true, keyProperty = "staff_id")
    @UpdateProvider(type = CommonProvider.class, method = "dynamicSQL")
    int batchUpdateByPrimaryKey(List<DemoStaff> recordList);

    @SuppressWarnings("MybatisMapperMethodInspection")
    @Options(useGeneratedKeys = true, keyProperty = "staff_id")
    @UpdateProvider(type = CommonProvider.class, method = "dynamicSQL")
    int batchUpdateByPrimaryKeySelective(List<DemoStaff> recordList);

    @SuppressWarnings("MybatisMapperMethodInspection")
    @Options(useGeneratedKeys = true, keyProperty = "staff_id")
    @DeleteProvider(type = IdsProvider.class, method = "dynamicSQL")
    int deleteByIds(String ids);
}